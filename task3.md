# Task 3:

Q: If you have multiple Ubuntu prod instances, How would you monitor them? What would be your
monitoring strategy?


A: I wouldn't bother too much with monitoring the instances themselves, but would rather have the main monitoring focus be the monitoring of the availability of the business service on those instances. That will be primary monitoring. For secondary monitoring, I'll focus on the usual metrics: CPU, Memory, Disk, etc. For monitoring software, I'll go with netdata as a easy first choice, and will only consider alternatives if new needs arrise, or if we outgrow the current monitoring solution. 

