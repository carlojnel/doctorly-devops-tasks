#
# outputs.tf
#

output "instance_public_ip" {
  value = aws_instance.doctorly-ubuntu.public_ip
}