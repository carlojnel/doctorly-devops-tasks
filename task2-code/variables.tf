#
# variables.tf
#


# Private key needed for remote exec provisioner
variable "ssh_priv_key_file" {
  default = "~/.ssh/id_rsa.2"
}
