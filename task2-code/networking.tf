#
# vpc.tf
#


# __   ___ __   ___
# \ \ / / '_ \ / __|
#  \ V /| |_) | (__
#   \_/ | .__/ \___|
#       |_|

## Note: Forgive my subnetting choices. I'm not a network engineer
## Subnet calc: https://www.davidc.net/sites/default/subnets/subnets.html?network=10.42.0.0&mask=16&division=21.f72300
resource "aws_vpc" "vpc-doctorrly-devops" {
  cidr_block           = "10.42.0.0/16"
  enable_dns_hostnames = true

}


#            _                _
#  ___ _   _| |__  _ __   ___| |_ ___
# / __| | | | '_ \| '_ \ / _ \ __/ __|
# \__ \ |_| | |_) | | | |  __/ |_\__ \
# |___/\__,_|_.__/|_| |_|\___|\__|___/
#
resource "aws_subnet" "subnet-main" {
  vpc_id     = aws_vpc.vpc-doctorrly-devops.id
  cidr_block = "10.42.1.0/24"
  map_public_ip_on_launch = "true"
}

#
#                           _ _
#  ___  ___  ___ _   _ _ __(_) |_ _   _    __ _ _ __ ___  _   _ _ __  ___
# / __|/ _ \/ __| | | | '__| | __| | | |  / _` | '__/ _ \| | | | '_ \/ __|
# \__ \  __/ (__| |_| | |  | | |_| |_| | | (_| | | | (_) | |_| | |_) \__ \
# |___/\___|\___|\__,_|_|  |_|\__|\__, |  \__, |_|  \___/ \__,_| .__/|___/
#                                 |___/   |___/                |_|
#


resource "aws_security_group" "sg_doctorly_ssh_all" {
  name        = "sg_doctorly_ssh"
  description = "Allow SSH to all"
  vpc_id      = aws_vpc.vpc-doctorrly-devops.id

  ingress {
    description = "Allow all inbound SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    description = "Allow all outbound egress"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

#
#  _       _                       _                 _
# (_)_ __ | |_ ___ _ __ _ __   ___| |_    __ _  __ _| |_ _____      ____ _ _   _
# | | '_ \| __/ _ \ '__| '_ \ / _ \ __|  / _` |/ _` | __/ _ \ \ /\ / / _` | | | |
# | | | | | ||  __/ |  | | | |  __/ |_  | (_| | (_| | ||  __/\ V  V / (_| | |_| |
# |_|_| |_|\__\___|_|  |_| |_|\___|\__|  \__, |\__,_|\__\___| \_/\_/ \__,_|\__, |
#                                        |___/                             |___/
#
resource "aws_internet_gateway" "igw-doctorly-pub" {
  vpc_id = aws_vpc.vpc-doctorrly-devops.id
}



#
#              _                 _
#  _ __   __ _| |_    __ _  __ _| |_ _____      ____ _ _   _
# | '_ \ / _` | __|  / _` |/ _` | __/ _ \ \ /\ / / _` | | | |
# | | | | (_| | |_  | (_| | (_| | ||  __/\ V  V / (_| | |_| |
# |_| |_|\__,_|\__|  \__, |\__,_|\__\___| \_/\_/ \__,_|\__, |
#                    |___/                             |___/
#
# and his EIP friend...





#
#                  _         _        _     _
#  _ __ ___  _   _| |_ ___  | |_ __ _| |__ | | ___  ___
# | '__/ _ \| | | | __/ _ \ | __/ _` | '_ \| |/ _ \/ __|
# | | | (_) | |_| | ||  __/ | || (_| | |_) | |  __/\__ \
# |_|  \___/ \__,_|\__\___|  \__\__,_|_.__/|_|\___||___/
#
#

resource "aws_route_table" "rtb-doctorly-pub" {
  vpc_id = aws_vpc.vpc-doctorrly-devops.id
}

resource "aws_route" "route-igw-doctorly-pub" {
  route_table_id         = aws_route_table.rtb-doctorly-pub.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.igw-doctorly-pub.id

}

resource "aws_route_table_association" "public-rt-association" {
  subnet_id      = aws_subnet.subnet-main.id
  route_table_id = aws_route_table.rtb-doctorly-pub.id
}