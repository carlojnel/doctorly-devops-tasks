#
# instances.tf
#

resource "aws_instance" "doctorly-ubuntu" {
  ami           = data.aws_ami.ubuntu_ami.id
  instance_type = "t3.nano"
  key_name      = "tf_ssh_deploy_key"
  ebs_optimized = "true"
  subnet_id     = aws_subnet.subnet-main.id
  vpc_security_group_ids = [
    aws_security_group.sg_doctorly_ssh_all.id
  ]

  associate_public_ip_address = true

  #   connection {
  #   user                = "ubuntu"
  #   host                = aws_instance.doctorly-ubuntu.public_ip
  #   private_key         = file(var.ssh_priv_key_file)
  #   agent               = true
  # }

  # provisioner "remote-exec" {
  #   inline = [
  #  "sudo apt update && sudo apt install software-properties-common && sudo add-apt-repository --yes --update ppa:ansible/ansible && sudo apt install -y ansible"
  #   ]
  # }

  root_block_device {
    volume_type = "gp3"
    volume_size = 8
  }

  lifecycle {
    ignore_changes = [ami]
  }

}

