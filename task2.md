# Task 2:

Q: 
How would you structure your Terraform project if you have multiple environments
and use different cloud providers?


A: 
It would depend on the project, team and the business application deployed. Broadly and loosely speaking though, I would as a start of with a single git repo containing my TF code, opting to seprate the different environment either via branches, or by using Terraform workspaces. Essentially I start of simple and basic, and then add complexity as needed to simplify things. Not trying to build the perfect Iron Man suit on my first try, but rather I'm aiming for a Janky but usable first version that I can improve on. 

A interesting write-up by Yevgeniy on managing multiple environments with TF can be found here: https://blog.gruntwork.io/how-to-manage-multiple-environments-with-terraform-32c7bc5d692

